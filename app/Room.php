<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function room(){
        return $this->belongsTo('App\Room_user');
    }
}
