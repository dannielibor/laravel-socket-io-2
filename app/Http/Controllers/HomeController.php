<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workspace;
use App\Workspace_user;
use App\Room;
use App\Room_user;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Auth::logout();
        $user = User::find(auth()->user()->id);
        $workspace = Workspace_user::where('user_id', auth()->user()->id)->where('status', 'active')->orderBy('workspace_id', 'desc')->get();
        $default_workspace = Workspace_user::where('user_id', '=', auth()->user()->id)->where('status', 'active')->orderBy('workspace_id', 'desc')->first();
        if($default_workspace){
            $room = Room_user::where('user_id', '=', auth()->user()->id)->where('status', 'active')->where('workspace_id', '=', $default_workspace->workspace_id)->get();
            $default_room = Room::where('name', '=', 'General')->orWhere('workspace_id', '=', $default_workspace->workspace_id)->first();
            $users = Room_user::where('room_id', $default_room->id)->where('status', 'active')->get();
        }else{
            $room = Room_user::where('user_id', '=', auth()->user()->id)->get();
            $default_room = "";
            $users = "";
        }
        $notify_workspace = Workspace_user::where('user_id', '=', auth()->user()->id)->where('status', 'pending')->get();
        $notify_room = Room_user::where('user_id', '=', auth()->user()->id)->where('status', 'pending')->get();
        return view('home')->with('users', $users)->with('notify_workspaces', $notify_workspace)->with('notify_rooms', $notify_room)->with('workspaces', $workspace)->with('user', $user)->with('default_workspace', $default_workspace)->with('rooms', $room)->with('default_room', $default_room);
    }
}
