<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Room_user;
use App\User;
use App\Workspace;
use App\Workspace_user;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'img' => 'image|nullable|max:1999'
        ]);
            // handle File Upload
        if($request->hasFile('img')){
            // Get filename with the extension
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            // Get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('img')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('img')->storeAs('public/images/rooms', $fileNameToStore);
        }else{
            $fileNameToStore = 'default_rooms.png';
        }

        $room = Room::where('name', $request->room)->get(); 

        if(count($room) < 1){

            $room = new Room();

            $room->name = $request->room;
            $room->workspace_id = $request->workspace_id;
            $room->type = $request->type;
            $room->img = $fileNameToStore;

            $room->save();

            $room = Room::where('name', $request->room)->first();

            $room_user = new Room_user();

            $room_user->isAdmin = 1;
            $room_user->user_id = auth()->user()->id;
            $room_user->workspace_id = $request->workspace_id;
            $room_user->room_id = $room->id;
            $room_user->status = 'active';

            $room_user->save();

        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = explode('.', $id);

        $user = User::find(auth()->user()->id);
        $workspace = Workspace_user::where('user_id', auth()->user()->id)->orderBy('workspace_id', 'desc')->get();
        $default_workspace = Workspace_user::where('user_id', '=', auth()->user()->id)->where('workspace_id', '=', $id[1])->first();
        $room = Room_user::where('user_id', '=', auth()->user()->id)->where('workspace_id', '=', $id[1])->get();
        $default_room = Room::where('id', $id[0])->first();
        $notify_workspace = Workspace_user::where('user_id', '=', auth()->user()->id)->where('status', 'pending')->get();
        $notify_room = Room_user::where('user_id', '=', auth()->user()->id)->where('status', 'pending')->get();
        $users = Room_user::where('room_id', $default_room->id)->where('status', 'active')->get();
        return view('home')->with('users', $users)->with('notify_workspaces', $notify_workspace)->with('notify_rooms', $notify_room)->with('workspaces', $workspace)->with('user', $user)->with('default_workspace', $default_workspace)->with('rooms', $room)->with('default_room', $default_room);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function invite_room(Request $request){
        $user = User::where('email', '=', $request->invite)->get();
        if(count($user) > 0){
            $workspace = Workspace_user::where('workspace_id', '=', $request->workspace_id)->where('user_id', '=', $user[0]->id)->get();
            if(count($workspace) > 0){
                $room = Room_user::where('workspace_id', '=', $request->workspace_id)->where('user_id', '=', $user[0]->id)->where('room_id', '=', $request->room_id)->get();
                if(count($room) > 0){
                    return 'error';
                }else{
                    $room = new Room_user();
                    $room->workspace_id = $request->workspace_id;
                    $room->room_id = $request->room_id;
                    $room->user_id = $user[0]->id;
                    $room->isAdmin = 0;
                    $room->status = "pending";
                    $room->save();
                }
            }else{
                return 'error in workspace';
            }
        }
        return back();
    }

    public function notify(Request $request){
        if($request->response == 'Accept'){

            $room = Room_user::where('room_id', $request->room_id)->where('user_id', auth()->user()->id)->first();
            $room->status = "active";
            $room->save();

        }else if($request->response == 'Reject'){
            $room = Room_user::where('room_id', $request->room_id)->where('user_id', auth()->user()->id)->first();
            $room->delete();
        }
        return back();
    }

}
