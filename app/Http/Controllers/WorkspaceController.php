<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workspace;
use App\Workspace_user;
use App\Room;
use App\Room_user;
use App\User;

class WorkspaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workspace = Workspace::orderBy('id', 'desc')->paginate(5);
        return view('home')->with('workspaces', $workspace);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'img' => 'image|nullable|max:1999'
        ]);
            // handle File Upload
        if($request->hasFile('img')){
            // Get filename with the extension
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            // Get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('img')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('img')->storeAs('public/images/workspaces', $fileNameToStore);
        }else{
            $fileNameToStore = 'default_workspace.png';
        }

        $workspace = Workspace::where('name', $request->name)->get();

        if(count($workspace) < 1){

            $workspace = new Workspace();
            $workspace->name = $request->name;
            $workspace->img = $fileNameToStore;
            $workspace->save();

            $workspace = Workspace::where('name', $request->name)->first();

            $workspace_user = new Workspace_user();
            $workspace_user->user_id = auth()->user()->id;
            $workspace_user->workspace_id = $workspace->id;
            $workspace_user->isAdmin = 1;
            $workspace_user->status = 'active';
            $workspace_user->save();

            $room = new Room();
            $room->workspace_id = $workspace->id;
            $room->save();

            $room = Room::where('name', '=', 'General')->where('workspace_id', '=', $workspace->id)->first();

            $room_user = new Room_user();
            $room_user->room_id = $room->id;
            $room_user->workspace_id = $workspace->id;
            $room_user->user_id = auth()->user()->id;
            $room_user->isAdmin = 1;
            $room_user->status = 'active';
            $room_user->save();

            return back()->with('success', 'Workspace created successfully');
        }else{
            return back()->with('error', 'Workspace already existing');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find(auth()->user()->id);
        $workspace = Workspace_user::where('user_id', auth()->user()->id)->orderBy('workspace_id', 'desc')->get();
        $default_workspace = Workspace_user::where('user_id', '=', auth()->user()->id)->where('workspace_id', '=', $id)->first();
        $room = Room_user::where('user_id', '=', auth()->user()->id)->where('workspace_id', '=', $id)->get();
        $default_room = Room::where('name', '=', 'General')->where('workspace_id', '=', $id)->first();
        $notify_workspace = Workspace_user::where('user_id', '=', auth()->user()->id)->where('status', 'pending')->get();
        $notify_room = Room_user::where('user_id', '=', auth()->user()->id)->where('status', 'pending')->get();
        $users = Room_user::where('room_id', $default_room->id)->where('status', 'active')->get();
        return view('home')->with('users', $users)->with('notify_workspaces', $notify_workspace)->with('notify_rooms', $notify_room)->with('workspaces', $workspace)->with('user', $user)->with('default_workspace', $default_workspace)->with('rooms', $room)->with('default_room', $default_room);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function invite_workspace(Request $request){
        $user = User::where('username', '=', $request->invite)->orWhere('email', '=', $request->invite)->get();
        if(count($user) > 0){
            $workspace = Workspace_user::where('workspace_id', '=', $request->workspace_id)->where('user_id', '=', $user[0]->id)->get();
            if(count($workspace) > 0){
                return back()->with('error', "User already exist");
            }else{
                $workspace = new Workspace_user();
                $workspace->user_id = $user[0]->id;
                $workspace->workspace_id = $request->workspace_id;
                $workspace->isAdmin = 0;
                $workspace->status = 'pending';
                $workspace->save();

                return back()->with('success', 'User has been invited');
            }
        }else{
            return back()->with('error', "User doesn't exist");
        }
    }

    public function notify(Request $request){

        if($request->response == 'Accept'){

            $workspace = Workspace_user::where('workspace_id', $request->workspace_id)->where('user_id', auth()->user()->id)->first();
            $workspace->status = "active";
            $workspace->save();

            $rooms = Room::where('workspace_id', '=', $request->workspace_id)->where('type', '=', 'public')->get();

                foreach($rooms as $room){
                    $room_user = new Room_user();
                    $room_user->workspace_id = $request->workspace_id;
                    $room_user->room_id = $room->id;
                    $room_user->user_id = auth()->user()->id;
                    $room_user->isAdmin = 0;
                    $room_user->status = 'active';
                    $room_user->save();
                }

        }else if($request->response == 'Reject'){
            $workspace = Workspace_user::where('workspace_id', '=', $request->workspace_id)->where('user_id', '=', auth()->user()->id)->first();
            $workspace->delete();
        }
        return back();
    }

}
