<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redis;

class MessageController extends Controller
{
    public function postMessage(Request $request)
    {
        $response = ['message' => $request->message];
        $redis = Redis::connection();
        $redis->publish('message', json_encode($response));
        return response()->json($response, 200);
    }
}
