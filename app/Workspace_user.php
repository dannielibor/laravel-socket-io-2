<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace_user extends Model
{
    public function workspace_user(){
        return $this->hasMany('App\Workspace', 'id', 'workspace_id');
    }
}
