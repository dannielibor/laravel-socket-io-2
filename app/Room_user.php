<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room_user extends Model
{
    public function room_user(){
        return $this->hasMany('App\Room', 'id', 'room_id');
    }
    public function user(){
        return $this->hasMany('App\User', 'id', 'user_id');
    }
}
