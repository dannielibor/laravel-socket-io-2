<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    public function workspace(){
        return $this->belongsTo('App\Workspace_user', 'workspace_id');
    }
}
