{{-- @extends('layouts.home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                <form action="/create_workspace" method = "get">
                    <div class="row">
                        <div class="col">
                            <input type="text" id = "name" name = "name" class="form-control" placeholder="Create new workspace">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary mb-2">Create</button>
                        </div>
                    </div>
                </form>
            <div class="card">
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if(count($workspaces) > 0)
                        @foreach($workspaces as $workspace)
                        <tr>
                            <th scope="row">{{$workspace->id}}</th>
                            <td><a href = "/">{{$workspace->name}}</a></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection --}}


@extends('layouts.home')

<style>
    .dropdown-item .user-item {
      white-space: normal;
      vertical-align: bottom;
    }
    </style>
    

@section('content')
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader" style = "background-color:#1f1f1f">
        <div class="loader">
            <div class="loader__figure" style = "border-color:#ff9900"></div>
            <p class="loader__label" style = "color:#ff9900">ProtoTEAM</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" style = "background-color:black">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" style = "background-color:#1f1f1f">
                <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-collapse">
                        <!-- ============================================================== -->
                        <!-- toggle and nav items -->
                        <!-- ============================================================== -->
                        <ul class="navbar-nav mr-auto">
                            <!-- This is  -->
                            <li class="nav-item hidden-sm-up"> <a class="nav-link nav-toggler waves-effect waves-light" href="javascript:void(0)"><i class="ti-menu"></i></a></li>
                            <!-- ============================================================== -->
                            <!-- Search -->
                            <!-- ============================================================== -->
                            <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)"><i class="fa fa-search"></i></a>
                                <form class="app-search">
                                    <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="fa fa-times"></i></a>
                                </form>
                            </li>
                        </ul>
                        <ul class="navbar-nav my-lg-0">
                            <!-- ============================================================== -->
                            <!-- User profile and search -->
                            <!-- ============================================================== -->
                            <li class="nav-item dropdown1">
                                {{-- <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../template/assets/images/users/1.jpg" alt="user" class="img-circle" width="30"></a> --}}
                               
                                        <a class="nav-link dropdown-toggle mr-lg-2 text-muted waves-effect waves-light" id="contactsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          
                                          <i class="fas fa-briefcase"></i>
                                          <span class="badge badge-pill badge-warning">
                                                @if(count($notify_workspaces) > 0)
                                                {{count($notify_workspaces)}}
                                                @else
                                                0
                                                @endif
                                            </span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" style="min-width: 300px;" aria-labelledby="contactsDropdown">
                                                @if(count($notify_workspaces) > 0)
                                                @foreach($notify_workspaces as $notify_workspace)
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">
                                                  <img src="/storage/images/workspaces/{{$notify_workspace->workspace_user[0]->img}}" class="rounded-circle" style="width: 50px; height: 50px;">
                                                  <div class="text-left user-item" style="display: inline-block; margin-left: 10px; width: 100px;">
                                                    {{$notify_workspace->workspace_user[0]->name}}
                                                  </div>
                                                  <span class="text-right">
                                                          <form action="/notify_workspace" method = "get">
                                                              <input type="hidden" id = "workspace_id" name = "workspace_id" value = "{{$notify_workspace->workspace_user[0]->id}}">
                                                      <input type="submit" class="btn btn-primary btn-sm" id = "response" name = "response" value="Accept">
                                                      <input type="submit" class="btn btn-default btn-sm" id = "response" name = "response" value="Reject">
                                                          </form>
                                                  </span>
                                                </a>
                                                @endforeach
                                                @endif
                                        </div>

                            </li>

                            <li class="nav-item dropdown2">
                                    <a class="nav-link dropdown-toggle mr-lg-2 text-muted waves-effect waves-light" id="contactsDropdown1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          
                                            <i class="fa fa-fw fa-bell"></i>
                                            <span class="badge badge-pill badge-warning">
                                                @if(count($notify_rooms) > 0)
                                                {{count($notify_rooms)}}
                                                @else
                                                0
                                                @endif
                                            </span>
                                            
                                          </a>
                                          <div class="dropdown-menu dropdown-menu-right" style="min-width: 300px;" aria-labelledby="contactsDropdown1">
                                              @if(count($notify_rooms) > 0)
                                              @foreach($notify_rooms as $notify_room)
                                              <div class="dropdown-divider"></div>
                                              <a class="dropdown-item" href="#">
                                                <img src="/storage/images/rooms/{{$notify_room->room_user[0]->img}}" class="rounded-circle" style="width: 50px; height: 50px;">
                                                <div class="text-left user-item" style="display: inline-block; margin-left: 10px; width: 100px;">
                                                  {{$notify_room->room_user[0]->name}}
                                                </div>
                                                <span class="text-right">
                                                        <form action="/notify_room" method = "get">
                                                            <input type="hidden" id = "room_id" name = "room_id" value = "{{$notify_room->room_user[0]->id}}">
                                                    <input type="submit" class="btn btn-primary btn-sm" id = "response" name = "response" value="Accept">
                                                    <input type="submit" class="btn btn-default btn-sm" id = "response" name = "response" value="Reject">
                                                        </form>
                                                </span>
                                              </a>
                                              @endforeach
                                              @endif
                                          </div>
                            </li>

                            <!-- ============================================================== -->
                            <!-- User profile and search -->
                            <!-- ============================================================== -->
                        </ul>
                        <ul class="navbar-nav my-lg-0">
                            <!-- ============================================================== -->
                            <!-- User profile and search -->
                            <!-- ============================================================== -->
                            <li class="nav-item dropdown3">
                                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/storage/images/users/{{$user->img}}" alt="user" class="img-circle" width="30"></a>
                                <div class="dropdown-menu dropdown-menu-right" style="min-width: 300px;" aria-labelledby="contactsDropdown2">
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">
                                      <img src="/storage/images/users/{{$user->img}}" class="rounded-circle" style="width: 50px; height: 50px;">
                                      <div class="text-left user-item" style="display: inline-block; margin-left: 10px; width: 100px;">
                                          @if($user->firstname == '' && $user->lastname == '')
                                          {{$user->username}}
                                          @else
                                          {{$user->firstname}} {{$user->lastname}}
                                          @endif
                                        
                                      </div>
                                    </a>
                                </div>
                            </li>
                            <!-- ============================================================== -->
                            <!-- User profile and search -->
                            <!-- ============================================================== -->
                        </ul>
    
                    </div>
                </nav>
            </header>
    
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" style = "opacity:0.98;background-color:#1f1f1f">
            <div class="d-flex no-block nav-text-box align-items-center" style = "background-color:#ff9900">
                <span><a href="#" class="btn waves-effect waves-light btn-success hidden-md-down" data-toggle="modal" data-target="#workspace" style = "width:100%;background-color:#1f1f1f;border:none;color:#ff9900">New workspace</a></span>
                <a class="waves-effect waves-dark ml-auto hidden-sm-down" href="javascript:void(0)" style = "color:#1f1f1f"><i class="ti-menu"></i></a>
                <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)" style = "color:#ff9900"><i class="ti-menu ti-close"></i></a>
            </div>
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        @foreach($workspaces as $workspace)
                        <li> <a class="waves-effect waves-dark" href="/workspace/{{$workspace->workspace_id}}" aria-expanded="false"><i>
                            <img src="/storage/images/workspaces/{{$workspace->workspace_user[0]->img}}" alt="user" class="img-circle" width="30"></i>
                            <span class="hide-menu">{{$workspace->workspace_user[0]->name}}</span></a>
                        </li>
                        @endforeach
                        {{-- <li> <a class="waves-effect waves-dark" href="index.html" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li> <a class="waves-effect waves-dark" href="pages-profile.html" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a></li>
                        <li> <a class="waves-effect waves-dark" href="table-basic.html" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu"></span>Tables</a></li>
                        <li> <a class="waves-effect waves-dark" href="icon-fontawesome.html" aria-expanded="false"><i class="fa fa-smile-o"></i><span class="hide-menu"></span>Icon</a></li>
                        <li> <a class="waves-effect waves-dark" href="map-google.html" aria-expanded="false"><i class="fa fa-globe"></i><span class="hide-menu"></span>Map</a></li>
                        <li> <a class="waves-effect waves-dark" href="pages-blank.html" aria-expanded="false"><i class="fa fa-bookmark-o"></i><span class="hide-menu"></span>Blank</a></li>
                        <li> <a class="waves-effect waves-dark" href="pages-error-404.html" aria-expanded="false"><i class="fa fa-question-circle"></i><span class="hide-menu"></span>404</a></li> --}}
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <div style = "background-color:white;margin-top:-5%;">
                    {{-- data-toggle="modal" data-target="#logout" --}}
                <a href="#" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" class = "btn" style = "text-align:left;width:260px;color:#1f1f1f" >Log out<i style = "padding-left:165px" class="fas fa-sign-out-alt"></i>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                         <!-- Logout Modal -->
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="logout">Logout</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-danger" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Continue</button>
            </div>
          </div>
        </div>
      </div>

            </div>
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper" style = "background-color:black">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div>
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!--content-->
                <div id="frame">
                        <div id="sidepanel" style = "background-color:black">
                            <div id="profile">
                                <div class="wrap">
                                    @if($default_workspace != '')
                                    <img id="profile-img" src="/storage/images/workspaces/{{$default_workspace->workspace_user[0]->img}}" class="away" alt="" />
                                    <p>{{$default_workspace->workspace_user[0]->name}}</p>
                                    @else
                                    <p>-----</p>
                                    @endif
                                    <i class="fa fa-chevron-down expand-button" style = "color:#ff9900" aria-hidden="true"></i>
                                    {{-- <div id="status-options">
                                        <ul>
                                            <li id="status-online" class="active"><span class="status-circle"></span> <p>Online</p></li>
                                            <li id="status-away"><span class="status-circle"></span> <p>Away</p></li>
                                            <li id="status-busy"><span class="status-circle"></span> <p>Busy</p></li>
                                            <li id="status-offline"><span class="status-circle"></span> <p>Offline</p></li>
                                        </ul>
                                    </div> --}}
                                    <div id="expanded">
                                        {{-- <label for="twitter"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></label>
                                        <input name="twitter" type="text" value="mikeross" />
                                        <label for="twitter"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></label>
                                        <input name="twitter" type="text" value="ross81" />
                                        <label for="twitter"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></label>
                                        <input name="twitter" type="text" value="mike.ross" /> --}}
                                        @if($default_workspace != '')
                                        <form action="/invite_workspace" method = "get">
                                        <input type="hidden" id = "workspace_id" value = "{{$default_workspace->workspace_id}}" name = "workspace_id">
                                        <input style = "background-color:white;color:#1f1f1f" placeholder="Enter Email" id="invite" type="text" class="form-control{{ $errors->has('invite') ? ' is-invalid' : '' }}" name="invite" value="{{ old('invite') }}" required>
                                        <input style = "background-color:#ff9900;color:#1f1f1f" type = "submit" class = "btn btn-success" value = "Send invite">
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div id="search" style = "background-color:#ff9900;color:#1f1f1f">
                                <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                                <input style = "background-color:#ff9900;color:#1f1f1f" type="text" placeholder="Search rooms..." />
                            </div>
                            <div id="contacts">
                                <ul>
                                    @if($default_workspace != '')
                                    @foreach($rooms as $room)
                                    @if($room->room_user[0]->name == 'General')
                                    <li class="contact active"><a href = "/room/{{$room->room_id}}.{{$default_workspace->workspace_id}}" style = "color:#1f1f1f">
                                            <div class="wrap">
                                                {{-- <span class="contact-status busy"></span> --}}
                                                <img src="/storage/images/rooms/{{$room->room_user[0]->img}}" alt="" />
                                                <div class="meta">
                                                        <p class="name">{{$room->room_user[0]->name}}</p>
                                                    <p class="preview">Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
                                                </div>
                                            </div></a>
                                        </li>
                                    @else
                                    <li class="contact"><a href = "/room/{{$room->room_id}}.{{$default_workspace->workspace_id}}" style = "color:white">
                                            <div class="wrap">
                                                {{-- <span class="contact-status busy"></span> --}}
                                                <img src="/storage/images/rooms/{{$room->room_user[0]->img}}" alt="" />
                                                <div class="meta">
                                                        <p class="name">{{$room->room_user[0]->name}}</p>
                                                    <p class="preview">Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
                                                </div>
                                            </div></a>
                                        </li>
                                    @endif
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                            <div id="bottom-bar">
                                <button data-toggle="modal" data-target="#room" id="addcontact" style = "background-color:#ff9900;color:#1f1f1f"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> <span>Add room</span></button>
                                <button id="settings" style = "background-color:#ff9900;color:#1f1f1f"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>
                            </div>
                        </div>
                        <div class="content">
                            <div class="contact-profile" style = "background-color:#ff9900;color:#1f1f1f">
                                @if($default_room != '')
                                <img src="/storage/images/rooms/{{$default_room->img}}" alt="" />
                                <p>
                                        {{$default_room->name}}
                                    </p>
                                @else
                                <p>
                                        -----
                                    </p>
                                @endif

                                <form class="form-inline offset-5" style = "padding:0%" method = "get" action = "/invite_room">
                                        
                                        <div class="form-group mx-sm-3 mb-2">
                                            @if($default_workspace != '')
                                            <input type="hidden" id = "workspace_id" value = "{{$default_workspace->workspace_id}}" name = "workspace_id">
                                            @endif
                                            @if($default_room != '')
                                            <input type="hidden" id = "room_id" value = "{{$default_room->id}}" name = "room_id">
                                            @endif
                                          <input type="text" class="form-control" id="invite" name = "invite" placeholder="Enter email">
                                        </div>
                                        <button type="submit" style = "background-color:#1f1f1f;color:#ff9900;border:none" class="btn btn-primary mb-2">Send invite</button>
                                        
                                        <ul class="navbar-nav my-lg-0" style = "margin-left:5%">
                                                <li class="nav-item dropdown3">
                                                       
                                                                <a class="nav-link dropdown-toggle mr-lg-2 text-muted" id="contactsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fas fa-user" style = "color:#1f1f1f"></i>
                                                                  <span style = "background-color:#1f1f1f;color:#ff9900" class="badge badge-pill badge-warning">
                                                                      @if($users != '')
                                                                        @if(count($users) > 0)
                                                                        {{count($users)}}
                                                                        @else
                                                                        0
                                                                        @endif
                                                                        @else
                                                                        0
                                                                    @endif
                                                                    </span>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right" style="min-width: 300px;" aria-labelledby="contactsDropdown">
                                                                        @if($users != '')    
                                                                    @if(count($users) > 0)
                                                                        @foreach($users as $user)
                                                                        <div class="dropdown-divider"></div>
                                                                        <a class="dropdown-item" href="#">
                                                                          <img src="/storage/images/users/{{$user->user[0]->img}}" class="rounded-circle" style="width: 50px; height: 50px;">
                                                                          <div class="text-left user-item" style="display: inline-block; margin-left: 10px; width: 100px;">
                                                                            {{$user->user[0]->username}}
                                                                          </div>
                                                                          {{-- <span class="text-right">
                                                                                <form action="/notify_workspace" method = "get">
                                                                            <input type="submit" class="btn btn-danger btn-sm" id = "response" name = "response" value="Kick">
                                                                                </form>
                                                                        </span> --}}
                                                                        </a>
                                                                        @endforeach
                                                                        @endif
                                                                        @else
                                                                        0
                                                                        @endif
                                                                </div>
                        
                                                    </li>
                                        </ul>
                                </form>

                                

                            </div>
                            <div class="messages" style = "background-color:#1f1f1f">
                                <ul>
                                    <li class="sent">
                                        <img src="/storage/images/users/default.jpg" alt="" />
                                        <p>How the hell am I supposed to get a jury to believe you when I am not even sure that I do?!</p>
                                    </li>
                                    <li class="replies">
                                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                                        <p>When you're backed against the wall, break the god damn thing down.</p>
                                    </li>
                                    <li class="replies">
                                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                                        <p>Excuses don't win championships.</p>
                                    </li>
                                    <li class="sent">
                                        <img src="/storage/images/users/default.jpg" alt="" />
                                        <p>Oh yeah, did Michael Jordan tell you that?</p>
                                    </li>
                                    <li class="replies">
                                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                                        <p>No, I told him that.</p>
                                    </li>
                                    <li class="replies">
                                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                                        <p>What are your choices when someone puts a gun to your head?</p>
                                    </li>
                                    <li class="sent">
                                        <img src="/storage/images/users/default.jpg" alt="" />
                                        <p>What are you talking about? You do what they say or they shoot you.</p>
                                    </li>
                                    <li class="replies">
                                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                                        <p>Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="message-input" style = "background-color:black">
                                <div class="wrap">
                                <input type="text" placeholder="Write your message..." />
                                <i class="fa fa-paperclip attachment" aria-hidden="true" style = "color:white"></i>
                                <button class="submit" style = "background-color:#ff9900;color:#1f1f1f"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
                <!-- room Modal -->
<div class="modal fade" id="room" tabindex="-1" role="dialog" aria-labelledby="room" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
                @if($default_workspace != '')
                <form method="post" action="/create_room" class = "was-validated" enctype = "multipart/form-data">
          <div class="modal-content">
            <div class="modal-header" style = "background-color:black;color:#ff9900">
              <h5 class="modal-title" id="room">New room  </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"style = "color:white">
                <span aria-hidden="true" >&times;</span>
              </button>
            </div>
            <div class="modal-body" style = "background-color:#1f1f1f;color:#ff9900">
                    
    
                            @csrf

                            <div class="form-row">
                    
                                    <div class="form-group col-md-12">
                          
                                        <label for="room">{{ __('Name') }}</label>
                          
                                        <input placeholder="Enter room" id="room" type="text" class="form-control{{ $errors->has('room') ? ' is-invalid' : '' }}" name="room" value="{{ old('room') }}" required>
                                        <input type="hidden" id = "workspace_id" name = "workspace_id" value = "{{$default_workspace->workspace_id}}">
                                        @if ($errors->has('room'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('room') }}</strong>
                                            </span>
                                        @endif
                          
                                    </div>
                                    <div class="form-group col-md-4">
                                            <label for="type">Type</label>
                                            <select id="type" name = "type" class="form-control">
                                              <option selected>Choose...</option>
                                              <option value = "public">Public</option>
                                              <option value = "private">Private</option>
                                            </select>
                                          </div>

                                          <div class = "form-group">
                                                <input type="file" id = "img" name = "img">
                                            </div>
                          
                            </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" style = "background-color:#1f1f1f;border:none;color:#ff9900">{{ __('Create') }}</button>
            </div>
        </form>
        @else
        <div class="modal-content">
                <div class="modal-header" style = "background-color:black;color:#ff9900">
                  <h5 class="modal-title" id="room">Oops  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"style = "color:white">
                    <span aria-hidden="true" >&times;</span>
                  </button>
                </div>
                <div class="modal-body" style = "background-color:#1f1f1f;color:#ff9900">
                        
                    <p>
                        No workspace found
                    </p>
        
                </div>
        @endif
          </div>
        </div>
    </div>
</div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <script type = "text/javascript">
    $(document).ready(function(){
    $(".send").keyup(function(e){alert('asd');
        // var msg = $(this).val();
        // var element = $(this);
        // if(!msg == '' && e.keyCode == 13 && !e.shiftKey){

        //     $.ajax({
        //         url: 'chat',
        //         type: 'post',
        //         data:{_token:'{{csrf_token()}}', msg:msg}, 
        //     });
        //     element.val('');
        // }
    });
});
    </script>
    <script src = "{{asset('js/app.js')}}"></script>
    <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script><script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script>

$(".messages").animate({ scrollTop: $(document).height() }, "fast");

$("#profile-img").click(function() {
	$("#status-options").toggleClass("active");
});

$(".expand-button").click(function() {
  $("#profile").toggleClass("expanded");
	$("#contacts").toggleClass("expanded");
});

$("#status-options ul li").click(function() {
	$("#profile-img").removeClass();
	$("#status-online").removeClass("active");
	$("#status-away").removeClass("active");
	$("#status-busy").removeClass("active");
	$("#status-offline").removeClass("active");
	$(this).addClass("active");
	
	if($("#status-online").hasClass("active")) {
		$("#profile-img").addClass("online");
	} else if ($("#status-away").hasClass("active")) {
		$("#profile-img").addClass("away");
	} else if ($("#status-busy").hasClass("active")) {
		$("#profile-img").addClass("busy");
	} else if ($("#status-offline").hasClass("active")) {
		$("#profile-img").addClass("offline");
	} else {
		$("#profile-img").removeClass();
	};
	
	$("#status-options").removeClass("active");
});

function newMessage() {
	message = $(".message-input input").val();
	if($.trim(message) == '') {
		return false;
	}

	$('<li class="sent"><img src="http://emilcarlsson.se/assets/mikeross.png" alt="" /><p>' + message + '</p></li>').appendTo($('.messages ul'));

    $.ajax({
    url: '/chat',
    type: 'get',
    data:{_token:'{{csrf_token()}}', msg:message}, 
});

	$('.message-input input').val(null);
	$('.contact.active .preview').html('<span>You: </span>' + message);
	$(".messages").animate({ scrollTop: $(document).height() }, "fast");
        
};

$('.submit').click(function() {
  newMessage();
});

$(window).on('keydown', function(e) {
  if (e.which == 13) {
    newMessage();
    return false;
  }
});
//# sourceURL=pen.js
</script>

<!-- Workspace Modal -->
<div class="modal fade" id="workspace" tabindex="-1" role="dialog" aria-labelledby="workspace" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
                <form method="post" action="/create_workspace" class = "was-validated" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header" style = "background-color:black;color:#ff9900">
              <h5 class="modal-title" id="workspace">New workspace  </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"style = "color:white">
                <span aria-hidden="true" >&times;</span>
              </button>
            </div>
            <div class="modal-body" style = "background-color:#1f1f1f;color:#ff9900">
                    
    
                            @csrf
                    
                            <div class="form-row">
                    
                                    <div class="form-group col-md-12">
                          
                                        <label for="name">{{ __('Name') }}</label>
                          
                                        <input placeholder="Enter workspace" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
                    
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                          
                                    </div>
                          
                          
                                  </div>

                            <div class = "form-group">
                                <input type="file" id = "img" name = "img">
                            </div>
    
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" style = "background-color:#1f1f1f;border:none;color:#ff9900">{{ __('Create') }}</button>
            </div>
        </form>
          </div>
        </div>


    @endsection