<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('workspace', 'WorkspaceController');
Route::resource('room_chat', 'RoomChatController');
Route::resource('room', 'RoomController');

Route::post('/create_workspace', 'WorkspaceController@store');

Route::get('/notify_workspace', 'WorkspaceController@notify');

Route::get('/notify_room', 'RoomController@notify');

Route::get('/invite_workspace', 'WorkspaceController@invite_workspace');

Route::get('/invite_room', 'RoomController@invite_room');

Route::post('/create_room', 'RoomController@store');

Route::get('/chat', 'RoomChatController@store');

Route::post('/post-message', 'MessageController@postMessage');